
package cr.ac.ucenfotec.proyecto.poo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Esta es la clase principal
 *
 * @author: Jorge Biramontes Calvo
 * @version: 1.45
 */

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {


        Parent root = FXMLLoader.load(getClass().getResource("InicioSesion.fxml"));
        Stage ventana = primaryStage;
        ventana.setTitle("El Baul de los recuerdos");
        ventana.setScene(new Scene(root, 400, 400));
        ventana.show();

    }

    public static void main(String[] args) {

        launch(args);
        //Controlador menu = new Controlador();
        //menu.desplegarMenu();
        /*Gestor nuevo=new Gestor();
        String fecha="1980-11-30";
        LocalDate fechanueva=nuevo.convertirFecha(fecha);
        int edad;
        edad=nuevo.calcularEdad(fechanueva);
        System.out.println(edad);
*/


    }


}

