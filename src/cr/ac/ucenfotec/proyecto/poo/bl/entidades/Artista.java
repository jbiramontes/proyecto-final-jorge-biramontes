package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

public class Artista {
    /**
     * Atributos de clase Artista
     */
private String descripcion;
private String nombreArtistico;
private String fechaDefuncion;
private Persona nuevoArtista=new Persona();


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreArtistico() {
        return nombreArtistico;
    }

    public void setNombreArtistico(String nombreArtistico) {
        this.nombreArtistico = nombreArtistico;
    }

    public String getFechaDefuncion() {
        return fechaDefuncion;
    }

    public void setFechaDefuncion(String fechaDefuncion) {
        this.fechaDefuncion = fechaDefuncion;
    }

    public Persona getNuevoArtista() {
        return nuevoArtista;
    }

    public void setNuevoArtista(Persona nuevoArtista) {
        this.nuevoArtista = nuevoArtista;
    }

    public Artista() {
    }

    public Artista(Persona nuevoArtista,String descripcion, String nombreArtistico, String fechaDefuncion ) {
        this.descripcion = descripcion;
        this.nombreArtistico = nombreArtistico;
        this.fechaDefuncion = fechaDefuncion;
        this.nuevoArtista = nuevoArtista;
    }

    @Override
    public String toString() {
        return "Artista{" +
                "descripcion='" + descripcion + '\'' +
                ", nombreArtistico='" + nombreArtistico + '\'' +
                ", fechaDefuncion='" + fechaDefuncion + '\'' +
                ", nuevoArtista=" + nuevoArtista +
                '}';
    }
}
