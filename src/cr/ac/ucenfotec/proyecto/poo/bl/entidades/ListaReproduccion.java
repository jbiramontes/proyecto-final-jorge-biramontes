package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

public class ListaReproduccion {
    /**
     * Atributos de clase ListaReproduccion
     */
    private String nombreLista;
    private String fechaCreacion;
    private Cancion cancionLista = new Cancion();
    private Usuario identificacionUsuario= new Usuario();

    public String getNombreLista() {
        return nombreLista;
    }

    public void setNombreLista(String nombreLista) {
        this.nombreLista = nombreLista;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Cancion getCancionLista() {
        return cancionLista;
    }

    public void setCancionLista(Cancion cancionLista) {
        this.cancionLista = cancionLista;
    }

    public Usuario getIdentificacionUsuario() {
        return identificacionUsuario;
    }

    public void setIdentificacionUsuario(Usuario identificacionUsuario) {
        this.identificacionUsuario = identificacionUsuario;
    }


    public ListaReproduccion() {
    }

    public ListaReproduccion(String nombreLista, String fechaCreacion, Cancion cancionLista, Usuario identificacionUsuario) {
        this.nombreLista = nombreLista;
        this.fechaCreacion = fechaCreacion;
        this.cancionLista = cancionLista;
        this.identificacionUsuario = identificacionUsuario;
    }

    @Override
    public String toString() {
        return "ListaReproduccion{" +
                "nombreLista='" + nombreLista + '\'' +
                ", fechaCreacion='" + fechaCreacion + '\'' +
                ", cancionLista=" + cancionLista +
                ", identificacionUsuario='" + getIdentificacionUsuario() + '\'' +
                '}';
    }


}
