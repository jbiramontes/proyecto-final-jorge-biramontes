package cr.ac.ucenfotec.proyecto.poo.bl.logica;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

import cr.ac.ucenfotec.proyecto.poo.bl.entidades.*;
import cr.ac.ucenfotec.proyecto.poo.dao.CancionDao;
import cr.ac.ucenfotec.proyecto.poo.dao.UsuarioDao;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class Gestor {
    ArrayList<Usuario> usuarios;
    ArrayList<Cancion> canciones;
    UsuarioDao nuevoUsuario = new UsuarioDao();

    public Gestor() {
        this.usuarios = new ArrayList<>();
        this.canciones = new ArrayList<>();
    }

    /**
     * Crea un nuevo objeto persona
     *
     * @param nombre nombre
     * @param apellidoUno primer apellido
     * @param apellidoDos segundo apellido
     * @param fechaNacimiento fecha nacimiento
     * @param edad edad se calcula sola
     * @param pais pais de nacimiento
     * @return un objeto Persona
     */
    public Persona crearPersona(String nombre, String apellidoUno, String apellidoDos, String fechaNacimiento, int edad,
                                String pais) {
        Persona nuevaPersona = new Persona(nombre, apellidoUno, apellidoDos, fechaNacimiento, edad, pais);
        return nuevaPersona;
    }

    /**
     * Metodo para crear un nuevo Usuario
     *
     * @param nuevaPersona objeto tipo Persona
     * @param id identifiacion
     * @param imagen avatar
     * @param correo correo
     * @param nombreUsuario nombre ficticio
     * @param contrasena palabra clave
     * @param tipoUsuario administrador o usuario
     * @return objeto Usuario
     */
    public Object agregarUsuario(Persona nuevaPersona, String id, String imagen, String correo, String nombreUsuario,
                                 String contrasena, String tipoUsuario) {
        Usuario nuevoUsuario = new Usuario(nuevaPersona, id, imagen, correo, nombreUsuario, contrasena, tipoUsuario);
        usuarios.add(nuevoUsuario);
        return nuevoUsuario;
    }

    /**
     * Metodo para crear objeto Cancion
     *
     * @param nombreCancion nombre de la cancion
     * @param califiacion calificacion de la cancion
     * @param idArtista identifiacion del artista
     * @param idGenero identificacion del genero
     * @param idCompositor identificacion del compositor
     * @return objeto Cancion
     */
    public Object agregarCancion(String nombreCancion, int califiacion, Artista idArtista, Genero idGenero,
                                 Compositor idCompositor) {
        Cancion nuevaCancion = new Cancion(nombreCancion, califiacion, idArtista, idGenero, idCompositor);
        canciones.add(nuevaCancion);
        return nuevaCancion;

    }

    public ArrayList<Usuario> listarUsuarios() {
        return this.usuarios;
    }
//Metodo aun no utilizable
    /*public void agregarArtista(Persona persona, String descripcion, String nombreArtistico, String fechaDefuncion
    ) {
        Artista nuevoArtista = new Artista(persona, descripcion, nombreArtistico, fechaDefuncion);
        artistas.add(nuevoArtista);
    }*/

    public ArrayList<Cancion> listarCanciones() {
        return this.canciones;
    }

    /**
     * Metodo para pasar un String a un LocalDate
     *
     * @param fecha recibe este parametro como un String
     * @return LocalDate
     */
    public LocalDate convertirFecha(String fecha) {
        try {
            return LocalDate.parse(fecha);
        } catch (Exception e) {
            System.out.println("Usted ha digitado la fecha en un formato incorrecto");

        }
        return null;
    }

    /**
     * Metodo para calculo de edad
     *
     * @param fechaNacimiento ingresado por usuario
     * @return la cantidad de años segun fecha nacimiento
     */
    public int calcularEdad(LocalDate fechaNacimiento) {
        LocalDate actual = LocalDate.now();
        Period periodo = Period.between(fechaNacimiento, actual);
        return periodo.getYears();
    }

    /**
     * Metodo para envio de correos
     *
     * @param correo recibe correo de usuario
     * @param nombre recibe nombre de usuario
     * @throws EmailException manejo de errores
     */
    public void enviarCorreo(String correo, String nombre) throws EmailException {
        try {
            Email email = new SimpleEmail();
            email.setSmtpPort(587);
            email.setAuthenticator(new DefaultAuthenticator("jbircalvo@gmail.com", "JORbir1980"));
            email.setDebug(false);
            email.setHostName("smtp.gmail.com");
            email.setFrom("jbircalvo@gmail.com");
            email.setSubject("Codigo verificacion El Baul de los Recuerdos");
            email.setMsg("Hola " + nombre + " su codigo de verificacion es 236571");
            email.addTo(correo);
            email.setTLS(true);
            email.send();
            System.out.println("Le hemos enviado un codigo de acceso a su correo");
        } catch (EmailException e) {
            System.out.println("El correo no pudo ser enviado");
        }
    }

    /**
     * Metodo para insertar objetos a la base de datos
     *
     * @param A es un objeto de de tipo Usuario o tipo Cancion
     */
    public void insertarDB(Object A) {
        if (A instanceof Usuario) {
            nuevoUsuario.InsertarUsuario(A);
        }
        if (A instanceof Cancion) {
            CancionDao nuevaCancion = new CancionDao();
            nuevaCancion.InsertarCancion(A);
        }


    }
}