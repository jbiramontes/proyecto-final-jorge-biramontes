package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

public class Usuario {
    /**
     * Atributos de clase Usuario
     */
    private String identificacion;
    private String imagen;
    private String correoUsuario;
    private String nombreUsuario;
    private String contrasena;
    private Persona personaUsuario = new Persona();
    private String tipoUsuario;


    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Persona getPersonaUsuario() {
        return personaUsuario;
    }

    public void setPersonaUsuario(Persona personaUsuario) {
        this.personaUsuario = personaUsuario;
    }

    public Usuario() {
    }

    public Usuario(Persona personaUsuario,String identificacion, String imagen, String correoUsuario, String nombreUsuario,
                   String contrasena,String tipoUsuario) {
        this.identificacion = identificacion;
        this.imagen = imagen;
        this.correoUsuario = correoUsuario;
        this.nombreUsuario = nombreUsuario;
        this.contrasena = contrasena;
        this.personaUsuario = personaUsuario;
        this.tipoUsuario= tipoUsuario;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "identificacion='" + identificacion + '\'' +
                ", imagen='" + imagen + '\'' +
                ", correoUsuario='" + correoUsuario + '\'' +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", contrasena='" + contrasena + '\'' +
                ", personaUsuario=" + personaUsuario
                ;
    }
}
