package cr.ac.ucenfotec.proyecto.poo.dao;

import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Cancion;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CancionDao {
    /**
     * Metodo para ingresar objetos a tabla tcanciones
     *
     * @param A objeto tipo Cancion
     */
    public void InsertarCancion(Object A) {

        try {
            System.out.println("Registrando Cancion espere un momento");
//Load the Driver
            String driver = "com.mysql.cj.jdbc.Driver";
            Class.forName(driver).newInstance();
            //System.out.println("LOADED DRIVER ---> " + driver);
// Create the "url"
// assume database server is running on the localhost
            String url = "jdbc:mysql://localhost:3306/bdAppBaulRecuerdos";
// Connect to the database represented by url
// with username root and password admin
            Connection con =
                    DriverManager.getConnection(url, "root", "root");
            //System.out.println("CONNECTED TO ---> " + url);
// Use the Connection to create a Statement object
            Statement stmt = con.createStatement();
//Insert data

            //System.out.println("INSERT INFO TO ---> " + url);
            boolean result = stmt.execute("insert into tcanciones (nombreCancion,calificacion,idArtista,idCompositor,idGenero)" +
                    "values ('" + ((Cancion) A).getNombreCancion() + "','" + ((Cancion) A).getCalificacion() + "'," +
                    "' Juan Collado'," +
                    "'Alberto Plaza'," +
                    "'Jose Cadmany')");

            stmt.close();
            con.close();
            System.out.println("Registro exitoso");
        } catch (
                SQLException e) {
            System.out.println("NO se pudo efectuar el registro" + e.toString());
        } catch (ClassNotFoundException e) {
            System.out.println("2" + e.toString());
        } catch (InstantiationException e) {
            System.out.println("3" + e.toString());
        } catch (IllegalAccessException e) {
            System.out.println("4" + e.toString());
        }

    }

}
