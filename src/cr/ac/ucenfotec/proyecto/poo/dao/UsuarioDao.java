package cr.ac.ucenfotec.proyecto.poo.dao;

import cr.ac.ucenfotec.proyecto.poo.bl.entidades.Usuario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UsuarioDao {
    /**
     * Metodo para insertar objetos a la tabla tusuario
     *
     * @param A objeto tipo Usuario
     */
    public void InsertarUsuario(Object A) {

        try {
            System.out.println("Espere un momento");
//Load the Driver
            String driver = "com.mysql.cj.jdbc.Driver";
            Class.forName(driver).newInstance();
            //  System.out.println("LOADED DRIVER ---> " + driver);
// Create the "url"
// assume database server is running on the localhost
            String url = "jdbc:mysql://localhost:3306/bdAppBaulRecuerdos";
// Connect to the database represented by url
// with username root and password admin
            Connection con =
                    DriverManager.getConnection(url, "root", "root");
            //System.out.println("CONNECTED TO ---> " + url);
// Use the Connection to create a Statement object
            Statement stmt = con.createStatement();
//Insert data

            //System.out.println("INSERT INFO TO ---> " + url);
            boolean result = stmt.execute("insert into tusuario (nombre,apellido_uno,apellido_dos,identificacion," +
                    "edad,nombreUsuario,contrasena,pais,fechaNacimiento,avatar)" +
                    "values ('" + ((Usuario) A).getPersonaUsuario().getNombre() + "','" + ((Usuario) A).getPersonaUsuario().getApellidoUno() + "'," +
                    "'" + ((Usuario) A).getPersonaUsuario().getApellidoDos() + "','" + ((Usuario) A).getIdentificacion() + "'," +
                    "'" + ((Usuario) A).getPersonaUsuario().getEdad() + "','" + ((Usuario) A).getNombreUsuario() + "'," +
                    "'" + ((Usuario) A).getContrasena() + "','" + ((Usuario) A).getPersonaUsuario().getPais() + "'," +
                    "'" + ((Usuario) A).getPersonaUsuario().getFechaNacimiento() + "','" + ((Usuario) A).getImagen() + "')");

            stmt.close();
            con.close();
            System.out.println("Registro exitoso");
        } catch (
                SQLException e) {
            System.out.println("NO se pudo efectuar el registro"/*+e.toString()*/);
        } catch (ClassNotFoundException e) {
            System.out.println("2" + e.toString());
        } catch (InstantiationException e) {
            System.out.println("3" + e.toString());
        } catch (IllegalAccessException e) {
            System.out.println("4" + e.toString());
        }

    }

}
