package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

public class Cancion {
    /**
     * Atributos de clase Cancion
     */
    private String nombreCancion;
    private int calificacion;
    private Artista artista = new Artista();
    private Genero generoCancion = new Genero();
    private Compositor compositorCancion = new Compositor();


    public String getNombreCancion() {
        return nombreCancion;
    }

    public void setNombreCancion(String nombreCancion) {
        this.nombreCancion = nombreCancion;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public Artista getArtista() {
        return artista;
    }

    public void setArtista(Artista artista) {
        this.artista = artista;
    }

    public Genero getGeneroCancion() {
        return generoCancion;
    }

    public void setGeneroCancion(Genero generoCancion) {
        this.generoCancion = generoCancion;
    }

    public Compositor getCompositorCancion() {
        return compositorCancion;
    }

    public void setCompositorCancion(Compositor compositorCancion) {
        this.compositorCancion = compositorCancion;
    }

    public Cancion() {
    }

    public Cancion(String nombreCancion, int calificacion, Artista artista, Genero generoCancion, Compositor compositorCancion) {

        this.nombreCancion = nombreCancion;
        this.calificacion = calificacion;
        this.artista = artista;
        this.generoCancion = generoCancion;
        this.compositorCancion = compositorCancion;
    }

    @Override
    public String toString() {
        return "Cancion{" +
                ", nombreCancion='" + nombreCancion + '\'' +
                ", calificacion=" + calificacion +
                ", artista=" + artista +
                ", generoCancion=" + generoCancion +
                ", compositorCancion=" + compositorCancion +
                '}';
    }
}
