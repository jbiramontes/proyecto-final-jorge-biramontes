package cr.ac.ucenfotec.proyecto.poo.bl.entidades;


public class Album {
    /**
     * Atributos de clase Album
      */
    private String nombreAlbum;
    private String fechaLanzamiento;
    private String imagen;
    private Cancion cancion = new Cancion();

    /**
     * Metodo para obtener nombre album
     * @return nombre album
     */
    public String getNombreAlbum() {
        return nombreAlbum;
    }

    /**
     * Set para nombre del album
     * @param nombreAlbum nombre de album
     */
    public void setNombreAlbum(String nombreAlbum) {
        this.nombreAlbum = nombreAlbum;
    }

    /**
     * Metodo para obtener fecha de lanzamiento
     * @return fecha de lanzamiento
     */
    public String getFechaLanzamiento() {
        return fechaLanzamiento;
    }

    /**
     * Set para fecha de lanzamiento
     * @param fechaLanzamiento fecha de lanzamiento
     */
    public void setFechaLanzamiento(String fechaLanzamiento) {
        this.fechaLanzamiento = fechaLanzamiento;
    }

    /**
     * Metodo para obtener imagen de album
     * @return imagen de album
     */
    public String getImagen() {
        return imagen;
    }

    /**
     * Set para imagen de album
     * @param imagen imagen
     */
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /**
     * Metodo para obtener cancion de album
     * @return cancion de album
     */
    public Cancion getCancion() {
        return cancion;
    }

    /**
     * Set para cancion de album
     * @param cancion cancion de album
     */
    public void setCancion(Cancion cancion) {
        this.cancion = cancion;
    }

    /**
     * Constructor sin parametros
     */
    public Album() {
    }

    /**
     * Constructor con parametros
     * @param nombreAlbum nombre album
     * @param fechaLanzamiento fecha lanzamiento
     * @param imagen imagen
     * @param cancion cancion
     */
    public Album(String nombreAlbum, String fechaLanzamiento, String imagen, Cancion cancion) {
        this.nombreAlbum = nombreAlbum;
        this.fechaLanzamiento = fechaLanzamiento;
        this.imagen = imagen;
        this.cancion = cancion;
    }

    /**
     * Metodo para extraer informacion de objeto album
     * @return informacion del album
     */
    @Override
    public String toString() {
        return "Album{" +
                "nombreAlbum='" + nombreAlbum + '\'' +
                ", fechaLanzamiento='" + fechaLanzamiento + '\'' +
                ", imagen='" + imagen + '\'' +
                ", cancion=" + cancion +
                '}';
    }
}
