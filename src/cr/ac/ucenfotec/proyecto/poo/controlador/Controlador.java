package cr.ac.ucenfotec.proyecto.poo.controlador;

import cr.ac.ucenfotec.proyecto.poo.Main;
import cr.ac.ucenfotec.proyecto.poo.bl.entidades.*;
import cr.ac.ucenfotec.proyecto.poo.bl.logica.Gestor;
import cr.ac.ucenfotec.proyecto.poo.dao.CancionDao;
import cr.ac.ucenfotec.proyecto.poo.iu.IU;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class Controlador {
    private IU Interfaz = new IU();
    private Gestor gestor = new Gestor();


    private void switchMenu(int opcion) {
        switch (opcion) {
            case 0:
                Interfaz.mensajes("---Te esperamos pronto---");
                break;
            case 1:
                crearUsuario();
                break;
            case 2:
                crearArtista();

                break;
            case 3:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 4:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 5:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 6:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 7:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 8:
                Interfaz.mensajes("---Lista de artistas registrados---");
                listarCanciones();
                break;
            case 9:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 10:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 11:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 12:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 13:
                Interfaz.mensajes("---Opcion no disponible por el momento digite las opciones 1,2,8 o 14---");
                break;
            case 14:
                Interfaz.mensajes("---Lista de usuarios registrados---");
                listarUsuarios();
                break;
            default:
                Interfaz.mensajes("Opcion no valida");
        }
    }

    /**
     * Proceso de registro solicita datos registro usuario
     */
    private void crearUsuario() {

        Persona nuevaPersona = crearPersona();
        if (nuevaPersona != null) {
            try {
                Interfaz.mensajes("Numero de identificacion ");
                String idUsuario = Interfaz.leerTexto();
                Interfaz.mensajes("Imagen ");
                String imagen = Interfaz.leerTexto();
                Interfaz.mensajes("Correo ");
                String correo = Interfaz.leerTexto();
                Interfaz.mensajes("Nombre usuario o Alias ");
                String nombreUsuario = Interfaz.leerTexto();
                Interfaz.mensajes("Contraseña ");
                String contresena = Interfaz.leerTexto();
                Interfaz.mensajes("Tipo usuario ");
                String tipoUsuario = Interfaz.leerTexto();
                gestor.insertarDB(gestor.agregarUsuario(nuevaPersona, idUsuario, imagen, correo, nombreUsuario, contresena, tipoUsuario));
                gestor.enviarCorreo(correo, nombreUsuario);

                //Interfaz.mensajes("Registro exitoso");
                System.exit(0);
            } catch (Exception e) {
                System.out.println("Usuario no registrado");
                System.exit(0);
            }
        } else {
            System.out.println("Usuario no registrado");
            System.exit(0);
        }
    }

    /**
     * Proceso de registro de canciones
     */
    private void crearCancion() {
        int opcion = 0;
        do {
            try {
                Interfaz.mensajes("Registro de nueva cancion");
                Interfaz.mensajes("Nombre de la cancion ");
                String nombreCancion = Interfaz.leerTexto();
                int calificacion = 6;
                do {
                    Interfaz.mensajes("Calificacion");

                    calificacion = Interfaz.leerOpcionMenu();

                    if (calificacion < 0 || calificacion > 5) {
                        System.out.println("La calificacion es de 0 a 5 solamente");
                    }
                } while (calificacion < 0 || calificacion > 5);

                Interfaz.mensajes("Ingrese el id del artista de la cancion");
                //Aqui falta Codigo para buscar id de artista en base de datos y extraerlo
                System.out.println("23");
                Artista artista = new Artista();
                Interfaz.mensajes("Ingrese el id del genero de la cancion");
                System.out.println("28");
                //Aqui falta Codigo para buscar id de genero en base de datos y extraerlo
                Genero generoCancion = new Genero();
                Interfaz.mensajes("Ingrese el id del compositor");
                System.out.println("89");
                //Aqui falta Codigo para buscar id del compositor en base de datos y extraerlo
                Compositor compositorCancion = new Compositor();
                gestor.insertarDB(gestor.agregarCancion(nombreCancion, calificacion,
                        artista, generoCancion, compositorCancion));


            } catch (Exception e) {
                System.out.println("Cancion no se pudo registar revise los datos ingresados");
                System.exit(0);
            }
            System.out.println("Desea registrar otra cancion?" + '\n' +
                    "Digite 0 para salir o cualquier numero para continuar");
            opcion = Interfaz.leerOpcionMenu();
        } while (opcion != 0);

        System.exit(0);

    }


    public void crearArtista() {

        Persona nuevaPersona = crearPersona();
        if (nuevaPersona != null) {
            try {
                Interfaz.mensajes("Descripcion del artista ");
                String descripcion = Interfaz.leerTexto();
                Interfaz.leerTexto();
                Interfaz.mensajes("Nombre artistico ");
                String nombreArtistico = Interfaz.leerTexto();
                Interfaz.mensajes("Fecha defuncion(Si aplica) ");
                String fechaDefuncion = Interfaz.leerTexto();
                //gestor.agregarArtista(nuevaPersona, descripcion, nombreArtistico, fechaDefuncion);
                Interfaz.mensajes("Artista registrado correctamente");
            } catch (Exception e) {
                System.out.println("El Artista no pudo ser registrado");
            }
        } else {
            System.out.println("El Artista no pudo ser registrado");
        }
    }

    public void listarUsuarios() {
        ArrayList<Usuario> usuarios = gestor.listarUsuarios();
        for (int i = 0; i < usuarios.size(); i++) {
            Interfaz.mensajes(usuarios.get(i).toString());
            Interfaz.mensajes("");
        }
    }

    public void listarCanciones() {
        ArrayList<Cancion> canciones = gestor.listarCanciones();
        for (int i = 0; i < canciones.size(); i++) {
            Interfaz.mensajes(canciones.get(i).toString());
            Interfaz.mensajes(" ");
        }
    }

    private Persona crearPersona() {
        try {
            Interfaz.mensajes("Ingrese los datos que se le solicitan: ");
            Interfaz.mensajes("Nombre  ");
            String nombre = Interfaz.leerTexto();
            Interfaz.mensajes("Primer apellido ");
            String apellidoUno = Interfaz.leerTexto();
            Interfaz.mensajes("Segundo apellido ");
            String apellidoDos = Interfaz.leerTexto();
            Interfaz.mensajes("Fecha nacimiento ");
            String fecha = Interfaz.leerTexto();
            LocalDate fechaNacimiento = gestor.convertirFecha(fecha);
            int edad = gestor.calcularEdad(fechaNacimiento);
            Interfaz.mensajes("Pais ");
            String pais = Interfaz.leerTexto();

            return gestor.crearPersona(nombre, apellidoUno, apellidoDos, fecha, edad, pais);
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * Metodo boton de registro Usuario
     *
     * @param actionEvent llama metodo crearUsuario
     */
    public void registro(ActionEvent actionEvent) {
        crearUsuario();

    }

    /**
     * Metodo boton registro cancion
     *
     * @param actionEvent llama metodo crearCancion
     * @throws Exception para manejo de errores en registro
     */
    public void registrarCancion(ActionEvent actionEvent) throws Exception {
        crearCancion();
    }

    public void Salir(ActionEvent actionEvent) {
        System.exit(0);
    }
}
