package cr.ac.ucenfotec.proyecto.poo.bl.entidades;

public class Compositor {
    /**
     * Atributos de clase Compositor
     */
    private Persona personaCompositor = new Persona();


    public Persona getPersonaCompositor() {
        return personaCompositor;
    }

    public void setPersonaCompositor(Persona personaCompositor) {
        this.personaCompositor = personaCompositor;
    }

    public Compositor() {
    }

    public Compositor(Persona personaCompositor) {
        this.personaCompositor = personaCompositor;
    }

    @Override
    public String toString() {
        return "Compositor{" +
                "personaCompositor=" + personaCompositor +
                '}';
    }
}
